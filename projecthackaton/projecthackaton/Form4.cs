﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using MySql.Data.MySqlClient;
using System.Drawing.Imaging;

namespace projecthackaton
{
    public partial class Form4 : Form
    {
        string name;
        string address;
        string birthdate;
        string gender;
        string edu;
        string reason;
        string placeremarks;
        string userreviews;
        string pic1;
        string pic2;
        string pic3;

        MySqlConnection con = new MySqlConnection("server=localhost;database=econfirm;userid=root;password=;");
        MySqlCommand cmd = new MySqlCommand();
        MySqlDataAdapter da = new MySqlDataAdapter();
        MySqlDataReader Rdr;


        public Form4(string name, string address, string birthdate, string gender, string edu, string reason, string placeremarks, string userreviews)
        {

            this.name = name;
            this.address = address;
            this.birthdate = birthdate;
            this.gender = gender;
            this.edu = edu;
            this.reason = reason;
            this.placeremarks = placeremarks;
            this.userreviews = userreviews;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form3 dd = new Form3(name, address, birthdate,  gender, edu, reason, placeremarks, userreviews);

            dd.Show();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            tname.Text = name;
            tadd.Text = address;
            tbir.Text = birthdate;
            tgen.Text = gender;
            tedu.Text = edu;
            trea.Text = reason;
            tpla.Text = placeremarks;
            tuse.Text = userreviews;
        }

        private void NEXT_Click(object sender, EventArgs e)
        {
            string add = name;

            //mysql

            MySqlCommand command = con.CreateCommand();
            cmd.CommandText = "INSERT INTO application (name, address, birthdate, gender,edach,reason,place,reviews) VALUES (@name, @address, @birthdate, @gender, @edach, @reason, @place, @reviews)";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@name", tname.Text);
            cmd.Parameters.AddWithValue("@address", tadd.Text);
            cmd.Parameters.AddWithValue("@birthdate", tbir.Text);
            cmd.Parameters.AddWithValue("@gender", tgen.Text);
            cmd.Parameters.AddWithValue("@edach", tedu.Text);
            cmd.Parameters.AddWithValue("@reason", trea.Text);
            cmd.Parameters.AddWithValue("@place", tpla.Text);
            cmd.Parameters.AddWithValue("@reviews", tuse.Text);

            
            cmd.Connection = con;

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();



            //adding pdf
            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream("char.pdf", FileMode.Create));
            doc.Open();


            iTextSharp.text.Image PNG = iTextSharp.text.Image.GetInstance("a.png");
             doc.Add(PNG);
         
            Paragraph paragraph = new Paragraph( "add?");

            doc.Add(paragraph);

            doc.Close();

            Form1 dd = new Form1();

            dd.Show();

            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Filter = "JPG Files(*.jpg)|*.jpg|PNG Files(*.png)|*.png|All Files(*.*)|*.*";

                if (dlg.ShowDialog() == DialogResult.OK)
                {


                    string picLoc = dlg.FileName.ToString();

                      pictureBox1.ImageLocation = picLoc;

                    //if (pictureBox1.Image == null)
                    //{

                       // pictureBox1.ImageLocation = picLoc;
                        //this.pictureBox1.Image = System.Drawing.Image.FromFile(picLoc).GetThumbnailImage(100, 100, null, IntPtr.Zero);
                        //picLoc = pic1;
                        //pictureBox1.Image.Save(@"C:\Users\gpjam_000\Documents\Visual Studio 2015\Projects\projecthackaton\projecthackaton\bin\Debug", System.Drawing.Imaging.ImageFormat.Jpeg);
                        //pictureBox1.Image.Save(@"C:\Users\gpjam_000\Documents\Visual Studio 2015\Projects\projecthackaton\projecthackaton\bin\Debug\sample.jpg", ImageFormat.Jpeg);

                    //}

                

                    //else if (pictureBox2.Image == null)
                    //{

                    //    this.pictureBox2.Image = System.Drawing.Image.FromFile(picLoc).GetThumbnailImage(100, 100, null, IntPtr.Zero);
                    //    picLoc = pic2;
                    //}
                    //else if (pictureBox3.Image == null)
                    //{

                    //    this.pictureBox3.Image = System.Drawing.Image.FromFile(picLoc).GetThumbnailImage(100, 100, null, IntPtr.Zero);
                    //    picLoc = pic3;
                    //}

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
