-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2016 at 06:02 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `econfirm`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `accno` int(11) NOT NULL,
  `curr` varchar(30) NOT NULL,
  `accnm` varchar(30) NOT NULL,
  `stat` varchar(30) NOT NULL,
  `availbal` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`accno`, `curr`, `accnm`, `stat`, `availbal`) VALUES
(1, 'peso', 'charls', 'confirm', '0'),
(2, 'peso', 'willis', 'confirmed', '0'),
(3, 'peso', 'inno', 'DENIED', '0'),
(4, 'peso', 'charls brian', 'confirm', '100000'),
(5, 'peso', 'name', 'confirm', '100000'),
(6, 'peso', 'brian', 'confirm', '100000'),
(7, 'peso', 'anderson', 'confirm', '100000');

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE IF NOT EXISTS `application` (
  `accno` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` varchar(30) NOT NULL,
  `birthdate` date NOT NULL,
  `gender` varchar(30) NOT NULL,
  `edach` varchar(30) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `place` varchar(50) NOT NULL,
  `reviews` varchar(30) NOT NULL,
  `loan` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`accno`, `name`, `address`, `birthdate`, `gender`, `edach`, `reason`, `place`, `reviews`, `loan`) VALUES
(0, 'inno', 'somewhere', '1992-02-26', 'male', 'college', 'educational purposes', 'in a far, far away land...', 'good payer', '');

-- --------------------------------------------------------

--
-- Table structure for table `claim`
--

CREATE TABLE IF NOT EXISTS `claim` (
  `chanid` int(11) NOT NULL,
  `transid` int(11) NOT NULL,
  `source` varchar(30) NOT NULL,
  `currency` varchar(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `accno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE IF NOT EXISTS `receipt` (
  `TransactNo` int(11) NOT NULL,
  `AccountNo` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Date` date NOT NULL,
  `amountcash` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`TransactNo`, `AccountNo`, `Name`, `Date`, `amountcash`) VALUES
(1, 0, '', '0000-00-00', 0),
(2, 0, 'label8', '0000-00-00', 0),
(3, 2, 'confirmed', '0000-00-00', 0),
(4, 3, 'DENIEDed', '0000-00-00', 0),
(5, 2, 'confirmed', '0000-00-00', 0),
(6, 2, 'willis', '0000-00-00', 0),
(7, 2, 'willis', '0000-00-00', 0),
(8, 2, 'willis', '0000-00-00', 0),
(9, 3, 'inno', '0000-00-00', 0),
(10, 3, 'inno', '0000-00-00', 0),
(11, 2, 'willis', '0000-00-00', 0),
(12, 2, 'willis', '0000-00-00', 0),
(13, 2, 'willis', '0000-00-00', 0),
(14, 2, 'willis', '0000-00-00', 0),
(15, 2, 'willis', '0000-00-00', 0),
(16, 1, 'charls', '0000-00-00', 0),
(17, 2, 'willis', '0000-00-00', 0),
(18, 1, 'charls', '0000-00-00', 0),
(19, 3, 'inno', '0000-00-00', 0),
(20, 2, 'willis', '0000-00-00', 0),
(21, 2, 'willis', '0000-00-00', 0),
(22, 2, 'willis', '0000-00-00', 0),
(23, 2, 'willis', '0000-00-00', 0),
(24, 2, 'willis', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `teller`
--

CREATE TABLE IF NOT EXISTS `teller` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `address` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teller`
--

INSERT INTO `teller` (`id`, `name`, `password`, `address`) VALUES
(1, 'inno', 'pass', 'cebu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`accno`);

--
-- Indexes for table `application`
--
ALTER TABLE `application`
  ADD PRIMARY KEY (`accno`);

--
-- Indexes for table `claim`
--
ALTER TABLE `claim`
  ADD PRIMARY KEY (`accno`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`TransactNo`);

--
-- Indexes for table `teller`
--
ALTER TABLE `teller`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `accno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `TransactNo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `teller`
--
ALTER TABLE `teller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
